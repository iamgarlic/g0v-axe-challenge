package h2.client

import anorm._ 
import play.api.db.DB
import play.api.Play.current

import bo._

object H2Client {

  def genId = System.currentTimeMillis // might conflict

  def test = {

    val _id = genId

    DB.withConnection { implicit c =>
      try {
        val id: Option[Long] = 
        SQL("insert into City(id, name, country) values ({id}, {name}, {country})")
        .on('id-> _id, 'name -> "Cambridge", 'country -> "New Zealand").executeInsert()
        println(s"id: ${_id}")
      }
      catch {
        case e: Exception => 
          println(e.getMessage)
      }
    }

    DB.withConnection { implicit c => 
      try{
        SQL("""select * from City cc where cc.id = {city};""").on('city-> _id).apply().map{ row => 
          City(id=row[Long]("id"), name=row[String]("name"), country=row[String]("country"))
        }.headOption match {
          case Some(theCity: City) => println(s"id: ${theCity.id} name: ${theCity.name}")
          case None => println(s"not found")
        }
      }catch{
        case e: Exception => 
          println(e.getMessage)
      }
    }
  }  
}