import play.api._
import play.api.mvc._
import play.filters.gzip.GzipFilter


object Filters {
  val gzByType = new GzipFilter(shouldGzip = (request, response) =>
  response.headers.get("Content-Type").exists(_.startsWith("application/json")))

  val gzByUri = new GzipFilter(shouldGzip = (request, response) =>  
  request.path.startsWith("/g0v/axe/lv1"))
}

object Global extends WithFilters(Filters.gzByUri) with GlobalSettings{
      
}
