package controllers

import play.api.mvc.{Action, Controller}
import play.api.libs.json.Json
import play.api.Routes

import play.api.libs.ws._
import scala.concurrent.{Future, Promise}
import scala.concurrent.duration._
import scala.util.{Success,Failure}
import play.api.libs.concurrent.Execution.Implicits._

object ExceptionController extends Controller {

  def randomBetween1to3000 = {
    val rnd = new scala.util.Random
    val range = 1000 to 3000
    range(rnd.nextInt(range length))
  }

  def randomBetween(a: Int, b: Int) = {
    val rnd = new scala.util.Random
    val range = a to b
    range(rnd.nextInt(range length))
  }

  def fOK: Future[String] = {
    val t = randomBetween1to3000
    Thread.sleep(t)
    Future { t.toString }
  }

  def fConditionOK(input:Int, check: Int => String): Future[String] = {
    Future{ check(input) }
  }

  def fNOK: Future[String] = {
    Thread.sleep(randomBetween(1,10))
    Future { 
      throw new Exception("nok") 
      "nok"
    }
  }  

  def fRandomOK: Future[String] = {
    val ran = randomBetween(1,100)
    Thread.sleep(ran)
    Future { 
      if(ran%2==0)
        throw new Exception(ran.toString) 
      else  
        ran.toString
    }
  }

  def fTimeout(d: FiniteDuration): Future[String] = model.TimeoutFuture(d) {  
    val to = randomBetween1to3000
    println(to)
    Thread.sleep(to)
    s"okBeforeTimeout:$to"
  }

  def fSleep(d: FiniteDuration): Future[String] = Future {
    Thread.sleep(d.toMillis)
    s"sleep:${d.toString}"
  }

  def fPlayTimeout(d: FiniteDuration): Future[java.util.concurrent.TimeoutException] = {
    println(s"playtimeout ${d.toMillis}")
    play.api.libs.concurrent.Promise.timeout(new java.util.concurrent.TimeoutException(), d)
  }


  def doOk = Action.async {
    fOK.map { response => 
      Ok(response).as("application/json")  
    }      
  }     

  def doNok = Action.async {

    fNOK.map { response => 
      // println("fNOK.map")
      Ok(response).as("application/json")  
    }.recover {
      case ex: Exception => 
        Ok("detectNOK").as("application/json")
    }

  }

  def doComposed = Action.async { 
    val fComposed = for {
        rok <- fOK
        rnok <- fNOK
      } yield {
        Ok("composedOK").as("application/json")
      }

    val fComposedRecover = fComposed.recover{
      case ex: Exception => 
        Ok("detectNOK").as("application/json")
    } 

    fComposedRecover

  }

  def doDoubleLayerComposed = Action.async { 
    
    fRandomOK.flatMap { response => 

      val fComposed = for {
          rok <- fRandomOK
          rnok <- fConditionOK(rok.toInt, { in =>
            if(in>50)
              throw new IndexOutOfBoundsException()
            else
              "%s%d".format("checked",in)
            })
        } yield {
          Ok("composedOK").as("application/json")
        }

      val fComposedRecover = fComposed.recover{
        case ex: IndexOutOfBoundsException => 
          Ok("outofbound_detect2ndLayerNOK").as("application/json")
        case ex: Exception => 
          Ok("ex_detect2ndLayerNOK").as("application/json")
      } 
      fComposedRecover

    }.recover {
      case ex: Exception => 
        Ok("detect1stLayerNOK").as("application/json")
    }
  }

  def doTimeout = Action.async {
    fTimeout(2 seconds).map { response => 
      // println("fTimeout.map")
      Ok(response).as("application/json")  
    }.recover {
      case tex: java.util.concurrent.TimeoutException => 
        Ok("detectTimeout").as("application/json")
      case ex: Exception => 
        Ok("detectNOK").as("application/json")
      case _ =>
        Ok("detectUnknownError").as("application/json")
    }
  }

  def doPlayTimeout(sleep: Long, timeout: Long) = Action.async {
    val st = new FiniteDuration(sleep, MILLISECONDS)
    val tt = new FiniteDuration(timeout, MILLISECONDS)
    Future.firstCompletedOf(Seq(fSleep(st), fPlayTimeout(tt))).map{
      case response: String =>         
        Ok(response).as("application/json")
      case tex: java.util.concurrent.TimeoutException =>            
        Ok("detectPlayTimeout").as("application/json")
      case _ =>   
        Ok("detectUnknownError").as("application/json")  
    }.recover{
      case _ =>   
        Ok("detectUnknownErrorInRecover").as("application/json")
    }
  } 


  def retry[T](number: Int)(f: => Future[T]): Future[T] = {
    
    f.map{ result: T => 
      println(s"in retry: $number $result")
      result
    }.recoverWith{
      case ex: Exception =>
        if(number==0)
          throw ex
        else  {
          println(s"trigger retry: $number")
          retry(number-1)(f)         
        }  
    }
  }


  def doRetry = Action.async {
    retry(3){fTimeout(2 seconds)}.map { response => 
      // println("fTimeout.map")
      Ok(response).as("application/json")  
    }.recover {
      case tex: java.util.concurrent.TimeoutException => 
        Ok("detectTimeoutAfterRetry").as("application/json")
      case ex: Exception => 
        Ok("detectNOKAfterRetry").as("application/json")
      case _ =>
        Ok("detectUnknownErrorAfterRetry").as("application/json")
    }
  }

  def doDeserialize = Action.async(parse.raw) { request =>
    val body = new String(request.body.asBytes(4096).get,"UTF-8")
    println("[api] request:%s".format(body))
    try {
      val p = jackson.json.deserialization[bo.Profile](body)
    }
    catch {
      case e => e.printStackTrace()
    }

    retry(1){fTimeout(2 seconds)}.map { response => 
      val p = jackson.json.deserialization[bo.Profile](body)
      p.age = 20
      p.country = "GB"
      Ok(jackson.json.serialize(p)).as("application/json")  
    }.recover {
      case tex: java.util.concurrent.TimeoutException => 
        Ok("detectTimeoutAfterRetry").as("application/json")
      case ex: Exception => 
        Ok("detectNOKAfterRetry").as("application/json")
      case _ =>
        Ok("detectUnknownErrorAfterRetry").as("application/json")
    }
  }

}