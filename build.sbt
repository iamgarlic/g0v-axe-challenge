name := """g0v-axe"""

version := "1.0-SNAPSHOT"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

scalaVersion := "2.11.1"

libraryDependencies ++= Seq(
  // Select Play modules
  "com.fasterxml.jackson.module" % "jackson-module-scala_2.10" % "2.4.2",
  "com.fasterxml.jackson.dataformat" % "jackson-dataformat-xml" % "2.4.2",
  // WebJars pull in client-side web libraries
  // "org.webjars" %% "webjars-play" % "2.2.0",
  "org.webjars" % "bootstrap" % "3.0.0",
  jdbc,      // The JDBC connection pool and the play.api.db API
  anorm,     // Scala RDBMS Library
  cache,
  ws
)

libraryDependencies += filters

