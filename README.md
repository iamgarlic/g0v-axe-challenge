# README #
This a web application based on [PlayFramework](https://www.playframework.com/).

## This is a project for studying [Scala](http://www.scala-lang.org/), [PlayFramework](https://www.playframework.com/), and [AKKA](http://akka.io/). ##

### Part One: [g0v](http://axe.g0v.tw/), practicing how to parse web page ###

* Extract data in a table from single page
* Extract data in a table from multiple page by page query string
* Extract data in a table from multiple page by session
* Extract data in a table from multiple page and robot detection mechanism is enabled in server side


### Part Two: future composition, timeout and retry ###

* /exception/ok, it's response depends on a Future which always success
* /exception/nok, it's response depends on a Future which always fail
* /exception/composed, it's response depends on two Futures. One always success and one always fail.
* /exception/timeout, it's response depends on a Future which has possibility to timeout
* /exception/playtimeout, it's response depends on a Future which has possibility to timeout (use the timeout mechanism defined in Play Promise object)
* /exception/retry, it's response depends on a Future which will retry when error happens
* /exception/doubleComposed, it's response depends on Futures executed sequentially and later output is related to previous one 