package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._
import play.api.http.ContentTypes.JSON

import scala.concurrent._
import play.api.libs.json.JsValue
import play.api.mvc.Results._
import play.api.mvc._

class AxeControllerSpec extends Specification {

  def validateResult(result: Future[Result]) = {
    // play.api.test.Helpers
    status(result) must equalTo(OK)
    contentType(result) must beSome("application/json")
    contentAsJson(result).isInstanceOf[JsValue] must equalTo(true)
  }
  
  "AxeController" should {
    
    "parseLv1 should return JSON" in new WithApplication {
      val result: Future[Result] = controllers.AxeController.parseLv1(FakeRequest())

      validateResult(result)    
    }

    "parseLv2 should return JSON" in new WithApplication {
      val result: Future[Result] = controllers.AxeController.parseLv2(FakeRequest())

      validateResult(result) 
    }

    "parseLv3 should return JSON" in new WithApplication {
      val result: Future[Result] = controllers.AxeController.parseLv3(FakeRequest())

      validateResult(result) 
    }

    "parseLv4 should return JSON" in new WithApplication {
      val result: Future[Result] = controllers.AxeController.parseLv4(FakeRequest())

      validateResult(result) 
    }

  }
}