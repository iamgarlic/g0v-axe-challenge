package test

import org.specs2.mutable._

import play.api.test._
import play.api.test.Helpers._
import play.api.http.ContentTypes.JSON

import scala.concurrent._
import play.api.libs.json.JsValue
import play.api.mvc.Results._
import play.api.mvc._

class AxeRouteSpec extends Specification {

  def validateResult(result: Future[Result]) = {
    status(result) must equalTo(OK)
    contentType(result) must beSome("application/json")
    contentAsJson(result).isInstanceOf[JsValue] must equalTo(true)
  }
  
  "AxeController" should {
    
    "parseLv1 should return JSON" in new WithApplication {
      val Some(result: Future[Result]) = route(FakeRequest(GET, "/g0v/axe/lv1"))

      validateResult(result)    
    }

    "parseLv2 should return JSON" in new WithApplication {
      val Some(result: Future[Result]) = route(FakeRequest(GET, "/g0v/axe/lv2"))

      validateResult(result) 
    }

    "parseLv3 should return JSON" in new WithApplication {
      val Some(result: Future[Result]) = route(FakeRequest(GET, "/g0v/axe/lv3"))

      validateResult(result) 
    }

    "parseLv4 should return JSON" in new WithApplication {
      val Some(result: Future[Result]) = route(FakeRequest(GET, "/g0v/axe/lv4"))

      validateResult(result) 
    }

  }
}